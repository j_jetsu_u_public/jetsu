################################################################################
#
# linux4tegra
#
################################################################################
LINUX4TEGRA_VERSION = $(call qstrip,$(BR2_PACKAGE_LINUX4TEGRA_REV).$(BR2_PACKAGE_LINUX4TEGRA_VER))

ifeq ($(BR2_PACKAGE_LINUX4TEGRA_PLATFORM_T186REF),y)
LINUX4TEGRA_SITE = https://developer.nvidia.com/embedded/l4t/r$(BR2_PACKAGE_LINUX4TEGRA_REV)_release_v$(BR2_PACKAGE_LINUX4TEGRA_VER)/t186
LINUX4TEGRA_SOURCE = jetson_linux_r$(LINUX4TEGRA_VERSION)_aarch64.tbz2
else # ifeq ($(BR2_PACKAGE_LINUX4TEGRA_PLATFORM_T210REF),y)
LINUX4TEGRA_SITE = https://developer.nvidia.com/embedded/l4t/r$(BR2_PACKAGE_LINUX4TEGRA_REV)_release_v$(BR2_PACKAGE_LINUX4TEGRA_VER)/t210
LINUX4TEGRA_SOURCE = jetson-210_linux_r$(LINUX4TEGRA_VERSION)_aarch64.tbz2
LINUX4TEGRA_EXCLUDES = bootloader/*.deb kernel/dtb/*.dtbo kernel/Image* kernel/*.tbz2 kernel/*.deb tools/*.deb tools/samplefs nv_tegra source
endif

LINUX4TEGRA_INSTALL_IMAGES = YES

define LINUX4TEGRA_INSTALL_IMAGES_CMDS
    cp -r $(@D)/. $(BINARIES_DIR)/linux4tegra
endef

$(eval $(generic-package))
