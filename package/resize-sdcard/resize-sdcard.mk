################################################################################
#
# resize-sdcard
#
################################################################################

RESIZE_SDCARD_VERSION = 1.0
RESIZE_SDCARD_SITE = $(BR2_EXTERNAL_jetsu_PATH)/package/resize-sdcard/source
RESIZE_SDCARD_SITE_METHOD = local

define RESIZE_SDCARD_INSTALL_TARGET_CMDS
    $(INSTALL) -D -m 0644 $(@D)/resize-sdcard.service $(TARGET_DIR)/etc/systemd/system/resize-sdcard.service
    $(INSTALL) -D -m 0755 $(@D)/resizefs.sh $(TARGET_DIR)/usr/bin/resizefs.sh
endef

$(eval $(generic-package))
