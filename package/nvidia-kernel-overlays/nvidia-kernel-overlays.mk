################################################################################
#
# nvidia-kernel-overlays
#
################################################################################

NVIDIA_KERNEL_OVERLAYS_VERSION = 0.1
NVIDIA_KERNEL_OVERLAYS_SITE = $(BR2_EXTERNAL_jetsu_PATH)/../linux4tegra/nvidia-kernel-overlays
NVIDIA_KERNEL_OVERLAYS_SITE_METHOD = local

define NVIDIA_KERNEL_OVERLAYS_APPLY_PATCHES
    $(APPLY_PATCHES) $(@D) $(BR2_EXTERNAL_jetsu_PATH)/package/nvidia-kernel-overlays \*.patch || exit 1;
endef

NVIDIA_KERNEL_OVERLAYS_POST_RSYNC_HOOKS += NVIDIA_KERNEL_OVERLAYS_APPLY_PATCHES

$(eval $(generic-package))
