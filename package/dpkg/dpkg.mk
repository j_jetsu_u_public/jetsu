################################################################################
#
# dpkg
#
################################################################################

DPKG_VERSION = 1.22.6
DPKG_SOURCE = dpkg_$(DPKG_VERSION).tar.xz
DPKG_SITE = http://ftp.de.debian.org/debian/pool/main/d/dpkg
DPKG_LICENSE = GPLv2+
DPKG_LICENSE_FILES = COPYING

DPKG_DEPENDENCIES = host-pkgconf libmd
DPKG_CONF_OPTS = \
    --localstatedir=/var \
    --disable-dselect \
    --disable-start-stop-daemon \
    --disable-update-alternatives \
    --disable-shared \
    --enable-static

HOST_DPKG_DEPENDENCIES = host-pkgconf host-libmd
HOST_DPKG_CONF_OPTS = \
    --disable-dselect \
    --disable-start-stop-daemon \
    --disable-update-alternatives \
    --disable-shared \
    --enable-static

$(eval $(autotools-package))
$(eval $(host-autotools-package))
