################################################################################
#
# nvidia-l4t-initrd
#
################################################################################

NVIDIA_L4T_INITRD_VERSION = $(call qstrip,$(BR2_PACKAGE_LINUX4TEGRA_REV).$(BR2_PACKAGE_LINUX4TEGRA_VER))
NVIDIA_L4T_INITRD_SITE = https://repo.download.nvidia.com
NVIDIA_L4T_INITRD_SOURCE = jetson
NVIDIA_L4T_INITRD_DEPENDENCIES = host-dpkg
NVIDIA_L4T_INITRD_INSTALL_IMAGES = YES

define NVIDIA_L4T_DPKG_DOWNLOAD
    rm -f $($(PKG)_DL_DIR)/$($(PKG)_SOURCE)
    $(WGET) -r -np -nH -P $($(PKG)_DL_DIR) \
        --accept-regex ".*/$(BR2_PACKAGE_LINUX4TEGRA_PLATFORM)/.*" \
        -A "$(call LOWERCASE,$(PKG))_$($(PKG)_VERSION)-[0-9]*_arm64.deb" \
        $($(PKG)_SITE)/$($(PKG)_SOURCE)
endef
NVIDIA_L4T_INITRD_POST_DOWNLOAD_HOOKS += NVIDIA_L4T_DPKG_DOWNLOAD

# Extract .deb file to build dir
define NVIDIA_L4T_INITRD_EXTRACT_CMDS
    $(HOST_DIR)/usr/bin/dpkg \
        -x $($(PKG)_DL_DIR)/$(shell ls $($(PKG)_DL_DIR) | sort -rd | head -1) \
        $(@D)/content
endef

define NVIDIA_L4T_INITRD_INSTALL_TARGET_CMDS
    rsync -avK $(@D)/content/* $(TARGET_DIR)
    rm -rf ${TARGET_DIR}/etc/ld.so.conf.d ${TARGET_DIR}/etc/ld.so.conf
endef

define NVIDIA_L4T_INITRD_INSTALL_IMAGES_CMDS
    cp $(@D)/content/boot/initrd $(BINARIES_DIR)/
endef

$(eval $(generic-package))
