################################################################################
#
# nvidia-l4t-core
#
################################################################################

NVIDIA_L4T_CORE_VERSION = $(call qstrip,$(BR2_PACKAGE_LINUX4TEGRA_REV).$(BR2_PACKAGE_LINUX4TEGRA_VER))
NVIDIA_L4T_CORE_SITE = https://repo.download.nvidia.com
NVIDIA_L4T_CORE_SOURCE = jetson
NVIDIA_L4T_CORE_DEPENDENCIES = host-dpkg

define NVIDIA_L4T_DPKG_DOWNLOAD
    rm -f $($(PKG)_DL_DIR)/$($(PKG)_SOURCE)
    $(WGET) -r -np -nH -P $($(PKG)_DL_DIR) \
        --accept-regex ".*/$(BR2_PACKAGE_LINUX4TEGRA_PLATFORM)/.*" \
        -A "$(call LOWERCASE,$(PKG))_$($(PKG)_VERSION)-[0-9]*_arm64.deb" \
        $($(PKG)_SITE)/$($(PKG)_SOURCE)
endef
NVIDIA_L4T_CORE_POST_DOWNLOAD_HOOKS += NVIDIA_L4T_DPKG_DOWNLOAD

# Extract .deb file to build dir
define NVIDIA_L4T_CORE_EXTRACT_CMDS
    $(HOST_DIR)/usr/bin/dpkg \
        -x $($(PKG)_DL_DIR)/$(shell ls $($(PKG)_DL_DIR) | sort -rd | head -1) \
        $(@D)/content
endef

define NVIDIA_L4T_CORE_INSTALL_TARGET_CMDS
    rsync -avK $(@D)/content/* $(TARGET_DIR)
    rm -rf ${TARGET_DIR}/etc/ld.so.conf.d ${TARGET_DIR}/etc/ld.so.conf
endef

$(eval $(generic-package))
