################################################################################
#
# nvidia-l4t-multimedia
#
################################################################################

NVIDIA_L4T_MULTIMEDIA_VERSION = $(call qstrip,$(BR2_PACKAGE_LINUX4TEGRA_REV).$(BR2_PACKAGE_LINUX4TEGRA_VER))
NVIDIA_L4T_MULTIMEDIA_SITE = https://repo.download.nvidia.com
NVIDIA_L4T_MULTIMEDIA_SOURCE = jetson
NVIDIA_L4T_MULTIMEDIA_DEPENDENCIES = host-dpkg libv4l

define NVIDIA_L4T_DPKG_DOWNLOAD
    rm -f $($(PKG)_DL_DIR)/$($(PKG)_SOURCE)
    $(WGET) -r -np -nH -P $($(PKG)_DL_DIR) \
        --accept-regex ".*/$(BR2_PACKAGE_LINUX4TEGRA_PLATFORM)/.*" \
        -A "$(call LOWERCASE,$(PKG))_$($(PKG)_VERSION)-[0-9]*_arm64.deb" \
        $($(PKG)_SITE)/$($(PKG)_SOURCE)
endef
NVIDIA_L4T_MULTIMEDIA_POST_DOWNLOAD_HOOKS += NVIDIA_L4T_DPKG_DOWNLOAD

# Extract .deb file to build dir
define NVIDIA_L4T_MULTIMEDIA_EXTRACT_CMDS
    $(HOST_DIR)/usr/bin/dpkg \
        -x $($(PKG)_DL_DIR)/$(shell ls $($(PKG)_DL_DIR) | sort -rd | head -1) \
        $(@D)/content
endef

define NVIDIA_L4T_MULTIMEDIA_INSTALL_TARGET_CMDS
    rsync -avK $(@D)/content/* $(TARGET_DIR)
    rm -rf ${TARGET_DIR}/etc/ld.so.conf.d ${TARGET_DIR}/etc/ld.so.conf
endef

# Replace default libv4l2 with Nvidia implementation
define NVIDIA_L4T_MULTIMEDIA_CREATE_SYMLINK
    ln -sf aarch64-linux-gnu/libv4l2.so.0.0.999999 $(TARGET_DIR)/usr/lib/libv4l2.so.0
    ln -sf libv4l2.so.0 $(TARGET_DIR)/usr/lib/libv4l2.so
    ln -sf aarch64-linux-gnu/libv4lconvert.so.0.0.999999 $(TARGET_DIR)/usr/lib/libv4lconvert.so.0
    ln -sf libv4lconvert.so.0 $(TARGET_DIR)/usr/lib/libv4lconvert.so
endef

NVIDIA_L4T_MULTIMEDIA_POST_INSTALL_TARGET_HOOKS += NVIDIA_L4T_MULTIMEDIA_CREATE_SYMLINK

$(eval $(generic-package))
