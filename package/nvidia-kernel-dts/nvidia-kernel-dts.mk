################################################################################
#
# nvidia-kernel-dts
#
################################################################################

NVIDIA_KERNEL_DTS_VERSION = 0.1
NVIDIA_KERNEL_DTS_SITE = $(BR2_EXTERNAL_jetsu_PATH)/../linux4tegra/nvidia-kernel-dts
NVIDIA_KERNEL_DTS_SITE_METHOD = local

$(eval $(generic-package))
