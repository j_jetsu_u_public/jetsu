################################################################################
#
# initd-service-wrapper
#
################################################################################

INITD_SERVICE_WRAPPER_VERSION = 1.0
INITD_SERVICE_WRAPPER_SITE = $(BR2_EXTERNAL_jetsu_PATH)/package/initd-service-wrapper/source
INITD_SERVICE_WRAPPER_SITE_METHOD = local

define INITD_SERVICE_WRAPPER_INSTALL_TARGET_CMDS
    $(INSTALL) -D -m 0755 $(@D)/service $(TARGET_DIR)/usr/bin/service
endef

$(eval $(generic-package))
