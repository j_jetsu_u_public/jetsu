#!/bin/bash

BOARD_DIR="$(dirname $0)"

install -m 0644 -D ${BOARD_DIR}/extlinux.conf ${TARGET_DIR}/boot/extlinux/extlinux.conf
install -m 0644 -D ${BOARD_DIR}/fstab ${TARGET_DIR}/etc/fstab

env_list=${BOARD_DIR}/system.env
envfile=${TARGET_DIR}/etc/environment
profile=${TARGET_DIR}/etc/profile
systemd_conf=${TARGET_DIR}/etc/systemd/system.conf
systemd_env=""

while IFS="=" read -ra env_var
do
    env_name=${env_var[0]}
    env_val=${env_var[1]}

    systemd_env="\"${env_name}=${env_val}\" ${systemd_env}"

    # Update /etc/environment
    if [ "$(grep -c "^${env_name}=*" "${envfile}")" == "0" ]; then
        echo "ENV: Add var $env_name"
        echo -e "${env_name}=${env_val}" >> ${envfile}
    elif [ "$(grep -c "^${env_name}=${env_val}$" "${envfile}")" == "0" ]; then
        echo "ENV: Update var $env_name"
        sed -i "s,^${env_name}=.*$,${env_name}=${env_val}," ${envfile}
    fi

    # Update /etc/profile
    if [ "$(grep -c "^export ${env_name}=*" "${profile}")" == "0" ]; then
        echo "PROFILE: Add var $env_name"
        echo -e "export ${env_name}=${env_val}" >> ${profile}
    elif [ "$(grep -c "^export ${env_name}=${env_val}$" "${profile}")" == "0" ]; then
        echo "PROFILE: Update var $env_name"
        sed -i "s,^export ${env_name}=.*$,export ${env_name}=${env_val}," ${profile}
    fi

done < "$env_list"

# Update systemd environment
if [ "$(grep -c "^DefaultEnvironment=*" "${systemd_conf}")" == "0" ]; then
    echo "SYSTEMD: Set DefaultEnvironment"
    echo -e "DefaultEnvironment=${systemd_env}" >> ${systemd_conf}
elif [ "$(grep -c "^DefaultEnvironment=${systemd_env}$" "${systemd_conf}")" == "0" ]; then
    echo "SYSTEMD: Update DefaultEnvironment"
    sed -i "s,^DefaultEnvironment=.*$,DefaultEnvironment=${systemd_env}," ${systemd_conf}
fi

# Disable systemd services
rm -f ${TARGET_DIR}/etc/systemd/system/multi-user.target.wants/nvmemwarning.service
rm -f ${TARGET_DIR}/etc/systemd/system/nvmemwarning.service
rm -f ${TARGET_DIR}/usr/lib/systemd/system/systemd-networkd-wait-online.service

# Provide builded rootfs to linux4tegra support package
cd ${BINARIES_DIR}/linux4tegra/bootloader && ln -sf ../../rootfs.ext4 ./system.img
